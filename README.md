# Installation
The Internal Analytics Client Api package should be installed via [Composer](http://getcomposer.org/doc/00-intro.md).  
To get the latest package version, add the repository in your composer.json and execute the following command from your project root:

```json
"repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/giftcartdev/api-client-internal-analytics"
    },
  ],
```

```sh
composer require tillo/api-client-internal-analytics
```

## Publishing in Laravel
After configuring your framework of choice, use the following command to publish the configuration settings:

```sh
php artisan vendor:publish --provider "Tillo\InternalAnalytics\ServiceProvider" --tag="config"
```

This will create the `config/internal-analytics.php` configuration file.

# End point parameters

### Sales Totals
```php
 $internalAnalytics->salesTotals(array $filters);
```  

| Filters            | Type        | Required   |               
|:-------------------|:------------|:-----------|
| partner_slug       | string      | Optional   |
| brand_slug         | string      | Optional   |
| brand_company_slug | string      | Optional   |
| agency_slug        | string      | Optional   |
| processor_slug     | string      | Optional   |
| currency           | iso3        | Optional   |
| transaction_type   | string      | Optional   |
| face_value_min     | number      | Optional   |
| face_value_max     | number      | Optional   |
| start_date         | Y-m-dTh:i:s | Yes        |
| end_date           | Y-m-dTh:i:s | Yes        |
 
 
### Brand totals by partner
 ```php
  $internalAnalytics->brandTotalsByPartner(array $filters);
 ```  
 
| Filters       | Type        | Required    |              
| :-------------|:------------|:----------- |
|  partnerSlug  | string      | Optional    |
|  currency     | iso3        | Optional    |
|  fromDate     | Y-m-dTh:i:s | Yes         |
|  toDate       | Y-m-dTh:i:s | Yes         |
 
### Partner totals by brand
```php
$internalAnalytics->partnerTotalsByBrand(array $filters);
```  

|Filters       | Type        | Required   |           
|:-------------|:------------|:-----------|
| partnerSlug  | string      | Optional   |
| currency     | iso3        | Optional   |
| fromDate     | Y-m-dTh:i:s | Yes        |
| toDate       | Y-m-dTh:i:s | Yes        |
 
### Partner brand totals by month for year
```php
$internalAnalytics->partnerBrandTotalsByMonthForYear(array $filters);
``` 

|Filters       | Type        | Required   |               
|:-------------|:------------|:-----------|
| partnerSlug  | string      | Optional   |
| brandSlug    | string      | Optional   |
| currency     | iso3        | Optional   |
| year         | Y (2019)    | Yes        |
 
### Partner brand totals by day for year month
```php
$internalAnalytics->partnerBrandTotalsByDayForYearMonth(array $filters);
``` 

|Filters       | Type        | Required     |             
|:-------------|:------------|:-----------|
| partnerSlug  | string      | Optional |
| brandSlug    | string      | Optional|
| currency     | iso3        | Optional|
| month        | m (10)      | Yes|
| year         | Y (2019)    | Yes|

### Partner brand totals by hour for year month day
```php
$internalAnalytics->partnerBrandTotalsByHourForYearMonthDay(array $filters);
```

|Filters       | Type        | Required   |             
|:-------------|:------------|:-----------|
| partnerSlug  | string      | Optional   |
| brandSlug    | string      | Optional   |
| currency     | iso3        | Optional   |
| day          | d (25)      | Yes        |
| month        | m (10)      | Yes        |
| year         | Y (2019)    | Yes        |

### Sales row count based on filters
```php
$internalAnalytics->salesRowCount(array $filters);
```

| Filters                 | Type            | Required     |
|:------------------------|:----------------|:-------------|
| partner_slug            | string          | Optional     |
| brand_slug              | string          | Optional     |
| agency_slug             | string          | Optional     |
| brand_company_slug      | string          | Optional     |
| currency                | iso3            | Optional     |
| transaction_type        | list of strings | Optional     |
| processor_slug          | string          | Optional     |
| sector                  | string          | Optional     |
| status                  | list of strings | Optional     |
| financial_relationship  | list of strings | Optional     |
| start_date              | Y-m-dTh:i:s     | Yes          |
| end_date                | Y-m-dTh:i:s     | Yes          |
| face_value_min          | number          | Optional     |
| face_value_max          | number          | Optional     |
| delivery_method         | string          | Optional     |
| tag                     | list of strings | Optional     |
| is_redemption           | string          | Optional     |
| redemption_status       | string          | Optional     |
| is_recognisable_revenue | string          | Optional     |
| flash_sale_code         | string          | Optional     |
