<?php
return [
    'apiBaseUri' => env('INTERNAL_ANALYTICS_BASE_URI'),
    'apiVersion' => env('INTERNAL_ANALYTICS_API_VERSION'),
    'apiUser'    => env('INTERNAL_ANALYTICS_API_USER'),
    'apiPwd'     => env('INTERNAL_ANALYTICS_API_PASSWORD'),
];