<?php

namespace Tillo\InternalAnalytics;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class InternalAnalytics
{
    /**
     * @var Client
     */
    private $client;
    private $lastResponse;


    /**
     * InternalAnalytics constructor.
     *
     * @param  Client  $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param  \GuzzleHttp\Psr7\Response  $response
     *
     * @return Response
     */
    protected function toResourceResponse(\GuzzleHttp\Psr7\Response $response)
    {
        return new Response($response);
    }

    /**
     * @param          $uri
     * @param  array   $params
     * @param  string  $method
     *
     * @return \GuzzleHttp\Psr7\Response
     * @throws GuzzleException
     */
    public function call($uri, $params = [], $method = 'GET')
    {
        try {
            $response           = $this->client->request($method, $uri, $params);
            $this->lastResponse = $response;
        } catch (GuzzleException $e) {
            report($e);
            throw $e;
        }


        return $response;
    }

    /**
     * # Filters:
     * * brand_slug : string
     * * currency: iso3
     * * denomination: string
     *
     * @param $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function stockLevels($filters)
    {
        $uri = 'stock_levels';

        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * # Filters:
     * * partner_slug : string
     * * brand_slug : string
     * * brand_company_slug : string
     * * agency_slug : string
     * * processor_slug : string
     * * currency: iso3
     * * transaction_type: string
     * * face_value_min: number
     * * face_value_max: number
     * * start_date: Y-m-dTh:i:s
     * * end_date: Y-m-dTh:i:s
     *
     * @param $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function salesTotals($filters)
    {

        $uri = 'sales_totals';

        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }


    /**
     *  # filters:
     *  * partner_slug : string
     *  * brand_slug : string
     *  * brand_company_slug : string
     *  * currency : string iso3
     *  * fromDate: string Y-m-dTh:i:s
     *  * toDate: string Y-m-dTh:i:s
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function faceValueSalesComparison(array $filters)
    {
        $uri       = 'face_value_sales_comparison';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     *  # filters:
     *  * partner_slug : string
     *  * brand_slug : string
     *  * brand_company_slug : string
     *  * currency : string iso3
     *  * fromDate: string Y-m-dTh:i:s
     *  * toDate: string Y-m-dTh:i:s
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function salesSnapshot(array $filters)
    {
        $uri       = 'sales_snapshot';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     *  # filters:
     *  * partnerSlug : string slug
     *  * currency : string iso3
     *  * fromDate: string Y-m-dTh:i:s
     *  * toDate: string Y-m-dTh:i:s
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function brandTotalsByPartner(array $filters)
    {
        $uri       = 'brand_totals_by_partner';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     *  # Filters:
     *  * partnerSlug : string slug
     *  * currency : string iso3
     *  * fromDate: string Y-m-dTh:i:s
     *  * toDate: string Y-m-dTh:i:s
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function partnerTotalsByBrand(array $filters)
    {
        $uri       = 'partner_totals_by_brand';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     *  # Filters:
     *  * partnerSlug : string slug
     *  * brandSlug : string slug
     *  * currency : string iso3
     *  * year: string Y (2019)
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function partnerBrandTotalsByMonthForYear(array $filters)
    {
        $uri       = 'partner_brand_totals_by_month_for_year';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }


    /**
     *  # Filters:
     *  * partnerSlug : string slug
     *  * brandSlug : string slug
     *  * currency : string iso3
     *  * month: string m (xx)
     *  * year: string Y (xxxx)
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function partnerBrandTotalsByDayForYearMonth(array $filters)
    {
        $uri       = 'partner_brand_totals_by_day_for_year_month';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     *  # Filters:
     *  * partnerSlug : string slug
     *  * brandSlug : string slug
     *  * currency : string iso3
     *  * day: string d (xx)
     *  * month: string m (xx)
     *  * year: string Y (xxxx)
     *
     * @param  array  $filters
     *
     * @return Response
     * @throws GuzzleException
     */
    public function partnerBrandTotalsByHourForYearMonthDay(array $filters)
    {
        $uri       = 'partner_brand_totals_by_hour_for_year_month_day';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function topPartners(array $filters)
    {
        $uri       = 'top_partners';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function topBrands(array $filters)
    {
        $uri       = 'top_brands';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function lincTotals(array $filters)
    {
        $uri       = 'linc_totals';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function lincRevenueVsSpend(array $filters)
    {
        $uri       = 'linc_revenue_vs_spend';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function lincRedemptionHours(array $filters)
    {
        $uri       = 'linc_redemption_hours';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

    /**
     * @param array $filters
     * @return Response
     */
    public function salesRowCount(array $filters)
    {
        $uri       = 'sales_row_count';
        $_response = $this->call($uri, ['query' => $filters]);
        $response  = $this->toResourceResponse($_response);

        return $response;
    }

}