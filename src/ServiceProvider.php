<?php

namespace Tillo\InternalAnalytics;

use App;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InternalAnalytics::class, function ($app) {
            $baseUri    = config('internal-analytics.apiBaseUri');
            $apiVersion = config('internal-analytics.apiVersion');
            $apiUser    = config('internal-analytics.apiUser');
            $apiPwd     = config('internal-analytics.apiPwd');

            $client = new Client([
                'verify'      => !App::environment('local'),
                'http_errors' => false,
                'auth'        => [$apiUser, $apiPwd],
                'timeout'     => 3,
                // Base URI is used with relative requests
                'base_uri'    => sprintf('%s/%s/', rtrim($baseUri, "/"), $apiVersion),
                'headers'     => [
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json',
                ],
            ]);

            return new InternalAnalytics($client);

        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [InternalAnalytics::class];
    }

}
